{
    "title_name":"Data and personalization",
    "title_description":"Manage connected applications, content you share and who can see",
    "application_name":"App Name",
    "application_name_desription":"App Name",
    "application_content":"App details",
    "application_content_description":"App content, production company, release date",
    "share_data_with":"Share",
    "share_data_with_description":"Who can see your posts",
    "blockade":"blockade",
    "blockade_description":"After you block someone, the other party will not be able to see the content you posted",
    "blocked_list":"Block List",
    "blocked_list_description":"Blockade name of the user"
}