{
    "title_name":"Setting",
    "title_description":"Setting bla bla bla",
    "merchant_info_icon":"mdiFolderInformationOutline",
    "merchant_info":"Merchant Information",
    "merchant_info_desription":"Provide personal details and how we can reach you",
    "data_and_personalization_icon":"mdiDatabase",
    "data_and_personalization":"Data & Personalization",
    "data_and_personalization_description":"Data & Personalization",
    "activity_icon":"mdiShoePrint",
    "activity":"Activity",
    "activity_description":"Review activity and joined events",
    "account_security_icon":"mdiShiedLock",
    "account_security":"Account Security",
    "account_security_description":"Update your password and secure your account",
    "payments_and_subscriptions_icon":"mdiCreditCardMultiple",
    "payments_and_subscriptions":"Payments & Subscriptions",
    "payments_and_subscriptions_description":"Review payments, payouts, coupons, gift cards, and taxes"
}