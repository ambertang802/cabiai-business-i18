{
    "title_name":"Login and Security",
    "title_description":"Update your password to secure your account",
    "update_password":"Change your password",
    "update_password_description":"We recommend the use of other services different strong password",
    "current_password":"Currently Password",
    "current_password_description":"Password is used",
    "new_password":"new password",
    "new_password_description":"Modified to the new password",
    "re_enter_new_password":"Re-enter the new password",
    "re_enter_new_password_description":"Re-enter the new password"
}