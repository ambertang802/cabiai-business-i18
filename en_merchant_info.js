{
    "title_name":"Merchant Information",
    "title_description":"Provide personal details and how we can reach you",
    "merchant_name":"The name of merchant",
    "merchant_name_desription":"The name of merchant",
    "address":"The address of merchant",
    "address_desciption":"The address of merchant",
    "email":"e-mail",
    "email_description":"Provide email for contact",
    "phone_number":"telephone number",
    "phone_number_description":"For contact merchant by phone",
    "date_commenced":"The day that start their business",
    "date_commenced_description":"The day that start their business",
    "date_applied":"The day that join our website",
    "date_applied_description":"The day that join our website"
}