{
    "title_name":"商戶資料",
    "title_description":"提供個人資料和聯絡方式",
    "merchant_name":"商戶名稱",
    "merchant_name_desription":"商戶名稱",
    "address":"商戶地址",
    "address_desciption":"商戶地址",
    "email":"電郵",
    "email_description":"提供電郵以作聯絡",
    "phone_number":"電話號碼",
    "phone_number_description":"提供電話號碼以作聯絡",
    "date_commenced":"營業日期",
    "date_commenced_description":"開業日期",
    "date_applied":"加入日期",
    "date_applied_description":"加入網頁日期"
}